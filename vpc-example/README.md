# Stack architecture
- Ref architecrure.drawio file

# Usage 使い方
- Download repository (this folder) -> Package and upload Cloudformation-template to S3 bucket -> Deploy to Cloudformation

## Download
- git clone ...

## Packge and upload to S3 bucket
- Create S3 bucket (If not exist)
```
aws s3 mb <input bucket-name>
```
- Package and upload to S3 bucket
```
aws cloudformation package --template-file scr/VPC-Stack-withEndpoints.yaml --s3-bucket <input bucket-name> --s3-prefix <input prefix> --output-template-file vpc-stack-bundle.yaml
```

## Deploy to Cloudformation
- Deploy stack
```
aws cloudformation deploy --stack-name <input stack-name> --capabilities CAPABILITY_IAM --template-file vpc-stack-bundle.yaml --parameter-overrides MainVpcCIDR="70.0.0.0/16" PeerVpcCIDR="70.1.0.0/16"
```